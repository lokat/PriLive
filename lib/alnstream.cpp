#include "alnstream.h"


//-------------------------------------------------------------------//
//------  The output Alignment Stream class  ------------------------//
//-------------------------------------------------------------------//

// new output Alignment Stream class
oAlnStream::oAlnStream(uint16_t ln, uint16_t tl, uint16_t cl, std::string rt, CountType rl, uint32_t nr, uint64_t bs, uint8_t fmt):
  lane(ln), tile(tl), cycle(cl), root(rt), rlen(rl), num_reads(nr), num_written(0), buffer(bs,0), buf_size(bs), buf_pos(0), format(fmt), ofile(NULL), ozfile(Z_NULL) {}


// write function for lz4 compression
uint64_t oAlnStream::lz4write(const char* source, uint64_t size) {
  // allocate buffer for the compressed data
  std::vector<char> buf (LZ4_COMPRESSBOUND(size),0);
  
  // compress the data
  uint32_t compressed_size = LZ4_compress (source, buf.data(), size);
  if (!compressed_size)
    throw std::runtime_error("Error compressing data with LZ4.");
  
  // write the block size
  if ( !fwrite(&compressed_size, 1, sizeof(uint32_t), ofile) )
    throw std::runtime_error("Error writing block size to file while compressing data with LZ4.");

  // write the data chunk
  if ( !fwrite(buf.data(), 1, compressed_size, ofile) )
    throw std::runtime_error("Error writing data to file while compressing with LZ4.");
  
  return size;
}


uint64_t oAlnStream::open(std::string fname) {
  // open the new Alignment file
  switch (format) {
  case 0: case 2: 
    ofile = fopen(fname.c_str(), "wb");
    if (!ofile) {
      std::cerr << "Could not open file " << fname << " for writing." << std::endl;
      return 0;
    }
    break;
  case 1:
    ozfile = gzopen(fname.c_str(), "wb1"); //Don't compress too much, not enough bang for the buck
    if (ozfile == Z_NULL) {
      std::cerr << "Could not open file " << fname << " for writing." << std::endl;
      return 0;
    }
    break;
  default:
    throw std::invalid_argument("Output file format not recognized");
  }

  // write the header:

  // calculate total size first
  unsigned long int total_size = 0;

  total_size += sizeof(uint16_t); // lane
  total_size += sizeof(uint16_t); // tile
  total_size += sizeof(CountType); // cycle

  // root directory name
  uint16_t root_size = root.size();
  total_size += sizeof(uint16_t);
  total_size += root_size;

  // read length
  total_size += sizeof(CountType);

  // number of reads
  total_size += sizeof(uint32_t);

  // create the vector to store the data
  std::vector<char> data (total_size);
  char* d = data.data();

  // write the lane
  memcpy(d,&lane,sizeof(uint16_t));
  d += sizeof(uint16_t);

  // write the tile
  memcpy(d,&tile,sizeof(uint16_t));
  d += sizeof(uint16_t);

  // write the cycle
  memcpy(d,&cycle,sizeof(CountType));
  d += sizeof(CountType);

  // root directory name
  memcpy(d,&root_size,sizeof(uint16_t));
  d += sizeof(uint16_t);

  memcpy(d,root.c_str(),root_size);
  d += root_size;

  // write the read length
  memcpy(d,&rlen,sizeof(CountType));
  d += sizeof(CountType);

  // write the number of reads
  memcpy(d,&num_reads,sizeof(uint32_t));
  d += sizeof(int32_t);

  // write all data
  uint64_t written = 0;
  switch (format) {
  case 0: case 2:  written = fwrite(data.data(), 1, data.size(), ofile); break;
  case 1: written = gzwrite(ozfile, data.data(), data.size()); break;
  }
  
  return written;
}


// writes a read alignment to the output Alignment file. 
// Buffering is handled internally
uint64_t oAlnStream::write_alignment(ReadAlignment * al) {
  if ( (!ofile && (format == 0 || format == 2)) || (ozfile == Z_NULL && format == 1) ){
    throw std::runtime_error("Could not write alignment to file. File handle not valid.");
  }
  if (num_written >= num_reads) {
    throw std::length_error("Could not write alignment to file. All alignments were already written.");
  }

  std::vector<char> data = al->serialize();
  uint64_t al_size = data.size();

  // first, write the size of the serialized alignment (uint32_t = 4 bytes)
  if (buf_pos+sizeof(uint32_t) <= buf_size) {
    // directly copy if all 4 bytes have space in the buffer (should be almost always the case)
    memcpy(buffer.data()+buf_pos,&al_size,sizeof(uint32_t));
    buf_pos += sizeof(uint32_t);
  } 
  else {
    // copy the first bytes into temporary buffer to compose the alignment size
    std::vector<char> temp (sizeof(uint32_t),0);
    memcpy(temp.data(),&al_size,sizeof(uint32_t));

    uint64_t first_part = buf_size-buf_pos;
    memcpy(buffer.data()+buf_pos,temp.data(),first_part);

    // write out buffer
    uint64_t written = 0;
    switch (format) {
    case 0: written = fwrite(buffer.data(), 1, buffer.size(), ofile); break;
    case 1: written = gzwrite(ozfile, buffer.data(), buffer.size()); break;
    case 2: written = lz4write(buffer.data(), buffer.size()); break;
    }
    if ( written != buf_size ) {
	    return 0;
    }

    // copy remaining data
    memcpy(buffer.data(),temp.data()+first_part,sizeof(uint32_t)-first_part);
    buf_pos = sizeof(uint32_t)-first_part;
  }

  // finally, write the serialized data
  uint64_t copied = 0;
  while (copied < al_size) {
    uint64_t to_copy = std::min(al_size-copied,buf_size-buf_pos);
    memcpy(buffer.data()+buf_pos, data.data()+copied, to_copy);
    buf_pos += to_copy;
    copied += to_copy;

    // write buffer to disk if full
    if(buf_pos >= buf_size){
      uint64_t written = 0;
      switch (format) {
      case 0: written = fwrite(buffer.data(), 1, buffer.size(), ofile); break;
      case 1: written = gzwrite(ozfile, buffer.data(), buffer.size()); break;
      case 2: written = lz4write(buffer.data(), buffer.size()); break;
      }
      if (written != buf_size) {
	      return 0;
      }
      buf_pos = 0;
    }
  }

  num_written++;

  return num_written;  
}


bool oAlnStream::close() {
  if ( ((format == 0 || format == 2) && ofile) || (format == 1 && ozfile != Z_NULL) ) {
    // write remaining buffer content to file
    uint64_t written = 0;
    switch (format) {
    case 0: written = fwrite(buffer.data(), 1, buf_pos, ofile); break;
    case 1: written = gzwrite(ozfile, buffer.data(), buf_pos); break;
    case 2: written = lz4write(buffer.data(), buf_pos); break;
    }

    if ( written != buf_pos ) {
	    return 0;
    }

    buf_pos = 0;
    if (num_written == num_reads) {
      switch (format) {
      case 0: case 2: fclose(ofile); break;
      case 1: gzclose(ozfile); break;
      }
      return true;
    }
    else {
      std::cerr << "Error: Could not close output alignment file! "<< num_reads - num_written <<" alignments missing." << std::endl;
      return false;
    }
  }
  else {
    std::cerr << "Error: Could not close output alignment file! File handle not valid." << std::endl;
    return false;
  }
}



//-------------------------------------------------------------------//
//------  The input Alignment Stream class  -------------------------//
//-------------------------------------------------------------------//

// new Alignment Stream class
iAlnStream::iAlnStream(uint64_t bs, uint8_t fmt):
  lane(0), tile(0), cycle(0), root(""), rlen(0), num_reads(0), num_loaded(0), buffer(bs,0), buf_size(bs), buf_pos(bs), format(fmt), ifile(NULL), izfile(Z_NULL) {}


// read function for lz4 decompression, reads one block of data
uint64_t iAlnStream::lz4read_block() {
  // get the size of the next block
  uint32_t compressed_size = 0;
  if ( !fread(&compressed_size,sizeof(uint32_t),1,ifile) )
    return 0;
  
  // allocate buffer for the compressed data
  std::vector<char> cbuf (compressed_size,0);

  // read the data
  if ( !fread(cbuf.data(),compressed_size,1,ifile) )
    throw std::runtime_error("Malformed input file. Could not read next block.");
  
  // decompress the data
  int64_t r_size = LZ4_decompress_safe (cbuf.data(), buffer.data(), compressed_size, buffer.size());
  if ( r_size < 0 )
    throw std::runtime_error("Error while decompressing LZ4 compressed block.");
  
  // update the current buffer size
  buf_size = r_size;

  return (uint64_t)r_size;
}


uint64_t iAlnStream::open(std::string fname) {
  // open the new Alignment file
  switch (format) {
  case 0: case 2:
    ifile = fopen(fname.c_str(), "rb");
    if (!ifile) {
      std::cerr << "Error opening file " << fname << " for reading." << std::endl;
      return 0;
    }
    break;
  case 1:
    izfile = gzopen(fname.c_str(), "rb");
    if (izfile == Z_NULL) {
      std::cerr << "Error opening file " << fname << " for reading." << std::endl;
      return 0;
    }
    break;
  default:
    throw std::invalid_argument("Input file format not recognized.");
  }

  // load the header:

  uint64_t bytes = 0;
  uint16_t root_size;
  switch (format) {
  case 0: case 2:
    {
      // read the lane
      bytes += fread(&lane,sizeof(uint16_t),1,ifile);
      // read the tile
      bytes += fread(&tile,sizeof(uint16_t),1,ifile);
      // read the cycle
      bytes += fread(&cycle,sizeof(CountType),1,ifile);
      // root directory name size
      bytes += fread(&root_size,sizeof(uint16_t),1,ifile);
      // root name
      char * tmp = new char[root_size+1];
      bytes += fread(tmp,1,root_size,ifile);
      tmp[root_size] = 0; // make the string null-terminated
      root = tmp;
      delete tmp;
      // read the read length
      bytes += fread(&rlen,sizeof(CountType),1,ifile);
      // read the number of reads
      bytes += fread(&num_reads,sizeof(uint32_t),1,ifile);
      break;
    }
  case 1:
    {
      // read the lane
      bytes += gzread(izfile,&lane,sizeof(uint16_t));
      // read the tile
      bytes += gzread(izfile,&tile,sizeof(uint16_t));
      // read the cycle
      bytes += gzread(izfile,&cycle,sizeof(CountType));
      // root directory name size
      bytes += gzread(izfile,&root_size,sizeof(uint16_t));
      // root name
      char * tmp = new char[root_size+1];
      bytes += gzread(izfile,tmp,root_size);
      tmp[root_size] = 0; // make the string null-terminated
      root = tmp;
      delete tmp;
      // read the read length
      bytes += gzread(izfile,&rlen,sizeof(CountType));
      // read the number of reads
      bytes += gzread(izfile,&num_reads,sizeof(uint32_t));
      break;
    }
  }

  return bytes;
}



ReadAlignment* iAlnStream::get_alignment(ReadAlignment* ra) {

  if ( (format==0 && !ifile) || (format==1 && izfile == Z_NULL) ){
    throw std::runtime_error("Could not load alignment from file. File handle not valid.");
  }
  if (num_loaded >= num_reads) {
    throw std::length_error("Could not load alignment from file. All alignments were already loaded.");
  }

  // first, get the size of the serialized alignment (uint32_t = 4 bytes)
  uint32_t al_size = 0;
  if (buf_pos+sizeof(uint32_t) <= buf_size) {
    // directly copy if all 4 bytes are in the buffer (should be almost always the case)
    memcpy(&al_size,buffer.data()+buf_pos,sizeof(uint32_t));
    buf_pos += sizeof(uint32_t);
  } 
  else {
    // copy the first bytes into temporary buffer to compose the alignment size
    std::vector<char> temp (sizeof(uint32_t),0);
    uint64_t first_part = buf_size-buf_pos;
    memcpy(temp.data(),buffer.data()+buf_pos,first_part);

    // load new buffer
    uint64_t loaded;
    switch (format) {
    case 0:
      loaded = fread(buffer.data(),1,buf_size,ifile);
      if ( ( loaded != buf_size ) && !feof(ifile) ) {
	      throw std::runtime_error("Unexpected error while loading alignment data from file.");
      }
      break;
    case 1:
      loaded = gzread(izfile,buffer.data(),buf_size);    
      if( ( loaded != buf_size ) && !gzeof(izfile) ) {
	      throw std::runtime_error("Unexpected error while loading alignment data from file.");
      }
      break;
    case 2:
      loaded = lz4read_block();    
      if ( loaded==0 && !feof(ifile) ) {
		throw std::runtime_error("Unexpected error while loading alignment data from file.");
      }
      break;
    }

    // copy remaining data and copy to variable
    memcpy(temp.data()+first_part,buffer.data(),sizeof(uint32_t)-first_part);
    buf_pos = sizeof(uint32_t)-first_part;
    memcpy(&al_size,temp.data(),sizeof(uint32_t));
  }
  
  // then, copy the content to the data vector
  std::vector<char> data(al_size,0);
  uint64_t copied = 0;
  while (copied < al_size) {
    uint64_t to_copy = std::min(al_size-copied,buf_size-buf_pos);
    memcpy(data.data()+copied, buffer.data()+buf_pos, to_copy);
    buf_pos += to_copy;
    copied += to_copy;

    // read new buffer from disk if necessary
    if(buf_pos >= buf_size){
      uint64_t loaded;
      switch (format) {
      case 0:
	loaded = fread(buffer.data(),1,buf_size,ifile);
	if ( (loaded != buf_size) && !feof(ifile) ) {
		throw std::runtime_error("Unexpected error while loading alignment data from file.");
	}
	break;
      case 1:
	loaded = gzread(izfile,buffer.data(),buf_size);    
	if ( ( loaded != buf_size) && !gzeof(izfile) ) {
		throw std::runtime_error("Unexpected error while loading alignment data from file.");
	}
	break;
      case 2:
	loaded = lz4read_block();    
	if ( loaded == 0 && !feof(ifile) ) {
		throw std::runtime_error("Unexpected error while loading alignment data from file.");
	}
	break;
      }
      buf_pos = 0;
    }
  }

  // finally, deserialize the alignment
//  ReadAlignment* ra = new ReadAlignment();
  ra->set_total_cycles(rlen);

  ra->deserialize(data.data());
  
  num_loaded++;

  return ra;
}


bool iAlnStream::close() {
  //if (ifile) {
  if ( ((format==0 || format==2) && ifile) || (format==1 && izfile != Z_NULL)) {
    if (num_loaded == num_reads) {
      switch (format) {
      case 0: case 2: fclose(ifile); break;
      case 1: gzclose(izfile); break;
      }
      return true;
    }
    else {
      std::cerr << "Error: Could not close alignment file! "<< num_reads - num_loaded <<" alignments missing." << std::endl;
      return false;
    }
  }
  else {
    throw std::runtime_error("Could not close alignment file. File handle not valid.");
  }
}




//-------------------------------------------------------------------//
//------  The StreamedAlignment class  ------------------------------//
//-------------------------------------------------------------------//

StreamedAlignment& StreamedAlignment::operator=(const StreamedAlignment& other) {
  if(&other == this)
    return *this;
  
  lane = other.lane;
  tile = other.tile;
  root = other.root;
  rlen = other.rlen;
  
  return *this;
}


std::string StreamedAlignment::get_bcl_file(uint16_t cycle, AlignmentSettings* settings, uint16_t read_number) {
  std::ostringstream path_stream;
  path_stream << root << "/L00" << lane << "/C" << getSeqCycle(cycle, settings, read_number) << ".1/s_"<< lane <<"_" << tile << ".bcl";
  return path_stream.str();
}


// get the path to the alignment file. The alignment file is located in
// <base>/L00<lane>/s_<lane>_<tile>.<cycle>.align
// if base == "": base = root
std::string StreamedAlignment::get_alignment_file(uint16_t cycle, uint16_t mate, std::string base){
  if (base == "") {
    base = root;
  }
  std::ostringstream path_stream;
  path_stream << base << "/L00" << lane << "/s_"<< lane << "_" << tile << "." << mate << "."<< cycle << ".align";
  return path_stream.str();
}


std::string StreamedAlignment::get_filter_file() {
  std::ostringstream path_stream;
  path_stream << root << "/L00" << lane << "/s_"<< lane << "_" << tile << ".filter";
  return path_stream.str();
}


// create directories required to store the alignment files (only if not stored in root)
void StreamedAlignment::create_directories(AlignmentSettings* settings) {
  std::ostringstream path_stream;
  if (settings->temp_dir == "") {
    path_stream << root;
  }
  else {
    path_stream << settings->temp_dir;
  }
  path_stream << "/L00" << lane;

  boost::filesystem::create_directories(path_stream.str());

  std::ostringstream sam_stream;
  sam_stream << settings->out_dir;
  sam_stream << "/L00" << lane;
  
  boost::filesystem::create_directories(sam_stream.str());
}



// initialize empty alignment. Creates alignment files for a virtual Cycle 0 (for all mates)
void StreamedAlignment::init_alignment(uint16_t mate, AlignmentSettings* settings) {
	std::string out_fname = get_alignment_file(0, mate, settings->temp_dir);

  // get the number of reads in this tile by looking in the first bcl file
  std::string first_cycle = get_bcl_file(1, settings, 0);

	// extract the number of reads
	uint32_t num_reads = num_reads_from_bcl(first_cycle);
  
	// open output alignment stream
	oAlnStream output (lane, tile, 0, root, rlen, num_reads, settings->block_size, settings->compression_format);
	output.open(out_fname);

	// write empty read alignments for each read
	for (uint32_t i = 0; i < num_reads; ++i) {
		ReadAlignment * ra = new ReadAlignment();
		ra->set_total_cycles(rlen);
		output.write_alignment(ra);
		delete ra;
	}

	if(!output.close()) {
		std::cerr << "Error: Could not create initial alignment file." << std::endl;
	}

} 



// extend an existing alignment from cycle <cycle-1> to <cycle>. returns the number of seeds
uint64_t StreamedAlignment::extend_alignment(uint16_t cycle, uint16_t read_no, uint16_t mate, KixRun* index, AlignmentSettings* settings, std::shared_ptr<IgnoreHandler> ignore_handler) {

  // 0. Init Ignore Handler if necessary
  //-------------------------
  if(settings->hasIgnoreAlignment() && !ignore_handler){
	ignore_handler = std::make_shared<IgnoreHandler>(lane, tile, cycle, mate, root, settings);
  }

  // 1. Open the input file
  //-----------------------
  std::string in_fname = get_alignment_file(cycle-1, mate, settings->temp_dir);

  std::string bcl_fname = get_bcl_file(cycle, settings, read_no);

  std::string filter_fname = get_filter_file();

  iAlnStream input ( settings->block_size, settings->compression_format );
  input.open(in_fname);
  assert(input.get_cycle() == cycle-1);
  assert(input.get_lane() == lane);
  assert(input.get_tile() == tile);
  assert(input.get_root() == root);
  assert(input.get_rlen() == rlen);

  uint32_t num_reads = input.get_num_reads();


  // 2. Open output stream
  //----------------------------------------------------------
  std::string out_fname = get_alignment_file(cycle, mate, settings->temp_dir);
  oAlnStream output (lane, tile, cycle, root, rlen, num_reads, settings->block_size, settings->compression_format);
  output.open(out_fname);


  
  // 3. Read the full BCL file (this is not too much)
  //-------------------------------------------------
  BclParser basecalls;
  basecalls.open(bcl_fname);
  
  // extract the number of reads from the BCL file
  uint32_t num_base_calls = basecalls.size();
  if ( num_base_calls != num_reads ) {
	throw std::runtime_error("Number of reads in BCL file does not match expected number of reads.");
  }

  if(ignore_handler)
	  // TODO: Is there a more elegant way than copying the data?
	  ignore_handler->setBasecalls(&basecalls); // needs to be copied to prevent deconstruction when outscoping.


  // 4. Load the filter flags if filter file is available
  // ----------------------------------------------------
  FilterParser filters;
  if (file_exists(filter_fname)) {
    filters.open(filter_fname);
    // extract the number of reads from the filter file
    uint32_t num_reads_filter = filters.size();
    
    if (num_reads != num_reads_filter){
      std::string msg = std::string("Number of reads in filter file (") + std::to_string(num_reads_filter) + ") does not match the number of reads in the BCL file (" + std::to_string(num_reads) + ").";
      throw std::length_error(msg.c_str());
    }
  }

  // 5. Extend alignments 1 by 1
  //-------------------------------------------------
  uint64_t num_seeds = 0;
  char bc;
  bool passed_filter;
  for (uint64_t i = 0; i < num_reads; ++i) {

	bc = basecalls.next();
	passed_filter = ( filters.size() > 0 && filters.has_next() ) ? filters.next() : true;

    ReadAlignment* ra = input.get_alignment();

	// If already discarded by ignore alignment, just increase cycle.
	if ( ra->flags == 0 && settings->hasIgnoreAlignment() && ! ignore_handler->isInMainAlignment(i) ) {
		ra->cycle += 1;
		output.write_alignment(ra);
		delete ra;
		continue;
	}

	CountType min_errors = 0;

	// extend alignments
	if ( !settings->hasIgnoreAlignment() || ignore_handler->isInMainAlignment(i) ) {

		// filter file available and filter information invalid
		if ( !passed_filter) {
			ra->cycle += 1;
			min_errors = std::numeric_limits<CountType>().max();
		}
		// filter file was not found or filter information valid
		else {
			ra->extend_alignment(bc, index, settings);
			min_errors = ra->min_errors;
		}
	} else if ( ! ignore_handler->isInMainAlignment(i) ) {
		min_errors = std::numeric_limits<CountType>().max();
	}

	// If too many errors, disable and allow discarding of read in RETIRED algorithm
	if ( min_errors > settings->min_errors) {

		ra->disable();
		if ( settings->hasIgnoreAlignment() )
			ignore_handler->updateForegroundAlignmentStatus(i, std::numeric_limits<CountType>().max());

	// If foreground alignment still fulfills criteria, update status and check whether the read must be discarded or not.
	} else if ( settings->hasIgnoreAlignment() ) {

		ignore_handler->updateForegroundAlignmentStatus(i, min_errors);

		// If read was discarded by the ignore alignment, stop reference alignment.
		if ( settings->hasIgnoreAlignment() && !(ignore_handler->isInMainAlignment(i)) ) {
			ra->disable();
		}
	}

	num_seeds += ra->seeds.size();

    output.write_alignment(ra);
    delete ra;
  }

  // 6. Close files
  //-------------------------------------------------
  if (!(input.close() && output.close())) {
    std::cerr << "Could not finish alignment!" << std::endl;
  }
  
  // 7. Delete old alignment file, if requested
  //-------------------------------------------
//  if ( ! settings->keep_aln_files ) {
//    std::remove(in_fname.c_str());
//  }
  if (ignore_handler)
	  ignore_handler->setBasecalls(&basecalls);
  return num_seeds;
}

void StreamedAlignment::extend_barcode(uint16_t bc_cycle, uint16_t read_cycle, uint16_t read_no, uint16_t mate, AlignmentSettings * settings) {

	// 1. Open the input file
	//-----------------------

	std::string in_fname = get_alignment_file(read_cycle, mate, settings->temp_dir);
	  std::string bcl_fname = get_bcl_file(bc_cycle, settings, read_no);
	  std::string filter_fname = get_filter_file();

	  iAlnStream input ( settings->block_size, settings->compression_format );
	  input.open(in_fname);
	  assert(input.get_cycle() == read_cycle);
	  assert(input.get_lane() == lane);
	  assert(input.get_tile() == tile);
	  assert(input.get_root() == root);

	  uint32_t num_reads = input.get_num_reads();


	  // 2. Open output stream
	  //----------------------------------------------------------
	  std::string out_fname = in_fname + ".temp";
	  oAlnStream output (lane, tile, read_cycle, root, input.get_rlen(), num_reads, settings->block_size, settings->compression_format);
	  output.open(out_fname);



	  // 3. Read the full BCL file (this is not too much)
	  //-------------------------------------------------
	  BclParser basecalls;
	  basecalls.open(bcl_fname);

	  // extract the number of reads from the BCL file
	  uint32_t num_base_calls = basecalls.size();
	  if ( num_base_calls != num_reads ) {
		throw std::runtime_error("Number of reads in BCL files does not match expected number of reads.");
	  }
	  
	  // 4. Extend barcode sequence
	  //-------------------------------------------------
	  for (uint64_t i = 0; i < num_reads; ++i) {
		char bc = basecalls.next() & 3; // only the nucleotide, ignore the quality.
	    ReadAlignment* ra = input.get_alignment();
	    ra->appendNucleotideToSequenceStoreVector(revtwobit_repr(bc), true);

	    // filter invalid barcodes if new barcode fragment is completed
	    // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
	    if ( !settings->keep_all_barcodes && bc_cycle == settings->seqs[read_no].length && ra->getBarcodeIndex(settings) == NO_MATCH ) {
	    	ra->disable();
	    }

	    output.write_alignment(ra);
	    delete ra;
	  }

	  // 5. Close files
	  //-------------------------------------------------
	  if (!(input.close() && output.close())) {
	    std::cerr << "Could not finish alignment!" << std::endl;
	  }

	  // 6. Move temp out file to the original file.
	  //-------------------------------------------
	  std::rename(out_fname.c_str(), in_fname.c_str());

}

bool StreamedAlignment::remove_old_alignment_file(uint16_t current_cycle, uint16_t current_mate, AlignmentSettings* settings){
	std::string path = get_alignment_file(current_cycle-1, current_mate, settings->temp_dir);
	return ( std::remove(path.c_str()) == 0 );
}


//-------------------------------------------------------------------//
//------  Streamed SAM generation -----------------------------------//
//-------------------------------------------------------------------//

uint64_t alignments_to_sam(uint16_t ln, uint16_t tl, std::string rt, CountType rl, CountType mate, KixRun* index, AlignmentSettings* settings) {

  SequenceElement seqEl = settings->getSeqByMate(mate);
  if ( seqEl == NULLSEQ || std::find(settings->exclude_mates.begin(), settings->exclude_mates.end(), mate) != settings->exclude_mates.end()) return 0;

  // set the file names
  std::string temp;
  if (settings->temp_dir == "")
    temp = rt;
  else
    temp = settings->temp_dir;

  std::string sam_dir = settings->out_dir;
  std::string filter_fname = filter_name(rt, ln, tl);
  std::string alignment_fname = alignment_name(ln, tl, rl, mate, temp);
  std::string sam_fname = sam_tile_name(sam_dir, ln, tl, mate, settings->write_bam);

  // check if files exist
  if ( !file_exists(alignment_fname) )
    throw std::runtime_error(std::string("Could not create SAM file. Alignment file not found: ")+ alignment_fname);
  if ( !file_exists(filter_fname) )
    std::cerr << "Could not find .filter file: " <<  filter_fname << std::endl;

  // open the alignment file
  iAlnStream input ( settings->block_size, settings->compression_format );
  input.open(alignment_fname);
  uint64_t num_reads = input.get_num_reads();

  // open the filter file, if applicable
  FilterParser filters;
  if (file_exists(filter_fname)) {
    filters.open(filter_fname);
    // extract the number of reads from the filter file
    uint32_t num_reads_filter = filters.size();

    if (num_reads != num_reads_filter){
      std::string msg = std::string("Number of reads in filter file (") + std::to_string(num_reads_filter) + ") does not match the number of reads in the BCL file (" + std::to_string(num_reads) + ").";
      throw std::length_error(msg.c_str());
    }
  }


  /////////////////////////////////////////////
  // generate sam file

  // set BamFileOut object
  seqan::CharString samFileName = sam_fname;
  seqan::BamFileOut samFileOut(seqan::toCString(samFileName));
  seqan::StringSet<seqan::CharString> referenceNames;
  for (unsigned i=0; i<seqan::length(index->seq_names); i++) {
      seqan::appendValue(referenceNames, index->seq_names[i]);
  }
  seqan::NameStoreCache<seqan::StringSet<seqan::CharString> > referenceNamesCache(referenceNames);
  seqan::BamIOContext<seqan::StringSet<seqan::CharString> > bamIOContext(referenceNames, referenceNamesCache);
  samFileOut.context = bamIOContext;

  /////////////////
  // set SAM header
  seqan::BamHeaderRecord headerRecord;

  // @HD header.
  seqan::clear(headerRecord);
  headerRecord.type = seqan::BAM_HEADER_FIRST;
  seqan::resize(headerRecord.tags, 2);
  headerRecord.tags[0].i1 = "VN";
  headerRecord.tags[0].i2 = "1.5";
  headerRecord.tags[1].i1 = "GO";
  headerRecord.tags[1].i2 = "query";
  seqan::writeHeader(samFileOut, headerRecord);

  // @SQ header.
  std::stringstream ss;
  for ( uint64_t i = 0; i < index->seq_names.size(); i++  ) {
    ss.str(std::string()); // clear string stream
    seqan::clear(headerRecord);
    headerRecord.type = seqan::BAM_HEADER_REFERENCE;
    seqan::resize(headerRecord.tags, 2);
        headerRecord.tags[0].i1 = "SN";
        headerRecord.tags[0].i2 = index->seq_names[i];

        headerRecord.tags[1].i1 = "LN";
        ss << index->seq_lengths[i];
        headerRecord.tags[1].i2 = ss.str();

    seqan::writeHeader(samFileOut, headerRecord);
  }

  // @PG header.
  seqan::clear(headerRecord);
  headerRecord.type = seqan::BAM_HEADER_PROGRAM;
  seqan::resize(headerRecord.tags, 3);
  headerRecord.tags[0].i1 = "ID";
  headerRecord.tags[0].i2 = "prilive";
  headerRecord.tags[1].i1 = "PN";
  headerRecord.tags[1].i2 = "PriLive";
  headerRecord.tags[2].i1 = "VN";
  ss.str(std::string());
  ss << PriLive_VERSION_MAJOR << "." << PriLive_VERSION_MINOR;
  headerRecord.tags[2].i2 = ss.str();
  seqan::writeHeader(samFileOut, headerRecord);

  /////////////////
  // prepare sam entries
  uint64_t num_alignments = 0;

  for (uint64_t i = 0; i < num_reads; ++i) {
    ReadAlignment * ra = input.get_alignment();

    // skip if a filter file exist and the next filter flag is set "invalid".
    if ( filters.size() > 0 && ! filters.next() ) {
        continue;
    }

    // Read name format <instrument‐name>:<run ID>:<flowcell ID>:<lane‐number>:<tile‐number>:<x‐pos>:<y‐pos>
    //readname << "<instrument>:<run-ID>:<flowcell-ID>:" << ln << ":" << tl << ":<xpos>:<ypos>:" << i;
    std::stringstream readname;
    readname << "lane." << ln << "|tile." << tl << "|read." << i;

    /////////////////
    // set sam entries
    seqan::BamAlignmentRecord record;
    bool printedFirstSeed = false;
    for (SeedVecIt it = ra->seeds.begin(); it != ra->seeds.end(); ) {
        seqan::clear(record);

        record.qName = readname.str();

        record.rID = (*it)->gid;

        record.beginPos = ra->get_SAM_start_pos(*it, *settings)-1; // seqan expects 0-based positions, but in PriLive we use 1-based
        if (record.beginPos < 0) {
            it = ra->seeds.erase(it);
            continue;
        }

        record.cigar = (*it)->returnSeqanCigarString();

        // flag and seq
        record.flag = 0;
        record.seq = ra->getSequenceString();
        if ((*it)->start_pos < 0) { // if read matched reverse complementary
            seqan::reverseComplement(record.seq);
            record.flag |= 16;
        }
        if (printedFirstSeed) { // if current seed is secondary alignment
            record.flag |= 256;
            seqan::clear(record.seq);
            record.qual = "*";
        }


        // check if cigar string sums up to read length
        unsigned cigarElemSum = 0;
        unsigned deletionSum = 0;
        for (seqan::Iterator<seqan::String<seqan::CigarElement<> > >::Type elem = seqan::begin(record.cigar); elem != end(record.cigar); ++elem) {
            if ((elem->operation == 'M') || (elem->operation == 'I') || (elem->operation == 'S') || (elem->operation == '=') || (elem->operation == 'X'))
                cigarElemSum += elem->count;
            if (elem->operation == 'D')
                deletionSum += elem->count;
        }
        if (cigarElemSum != seqEl.length) {
            std::cerr << "WARNING: Excluded an alignment of read " << record.qName << " at position " << ra->get_SAM_start_pos(*it, *settings) << " because its cigar vector had length " << cigarElemSum << std::endl;
            it = ra->seeds.erase(it);
            continue;
        }
        if (deletionSum >= seqEl.length) {
            std::cerr << "WARNING: Excluded an alignment of read " << record.qName << " at position " << ra->get_SAM_start_pos(*it, *settings) << " because its cigar vector had " << deletionSum << " deletions" << std::endl;
            it = ra->seeds.erase(it);
            continue;
        }

        // tags
        seqan::BamTagsDict dict;
        seqan::appendTagValue(dict, "AS", (*it)->num_matches);
        if (ra->getBarcodeString()!="") {// if demultiplexing is on

                // insert "-" as delimiter between the single barcodes
                uint16_t bc_counter = 0;
                std::string barcode = ra->getBarcodeString();
                for ( uint16_t i = 0; i != settings->seqs.size(); i++) {
                        if ( settings->getSeqById(i).isBarcode() ) {
                                bc_counter += settings->getSeqById(i).length;
                                barcode.insert(bc_counter, "-");
                                bc_counter++;
                        }
                }
                barcode = barcode.substr(0, barcode.size()-1); // remove the latest -
            seqan::appendTagValue(dict, "BC", barcode);
        }
        seqan::appendTagValue(dict, "NM", deletionSum + seqEl.length - (*it)->num_matches);
        record.tags = seqan::host(dict);

        // write record to disk
        seqan::writeRecord(samFileOut, record);
        ++num_alignments;
        printedFirstSeed = true;
        ++it;
    }
  }

  std::ofstream statsfile;
  statsfile.open(sam_fname+".stats");
  statsfile << "Number of reads\t" << num_reads << std::endl;
  statsfile << "Number of alignments\t" << num_alignments << std::endl;
  statsfile.close();

  return 0;
}

