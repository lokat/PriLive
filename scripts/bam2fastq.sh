#!/bin/bash

help="Convert bam files to FASTQ format. Appends NM:i tag to the header. The output of the script is appended to the output file. For overwriting, please remove the file beforehand.\n\nREQUIRED PARAMETERS:\n\t-i\tInput file (BAM format)\n\t-o\tOutput file (.fq or .fastq)\n\nOPTIONAL PARAMETERS:\n\t-f\tPattern to only get specified reads (simple grep pattern)\n\nGENERAL PARAMETERS:\n\t-h\tPrint help\n"
filter="^";

while getopts i:o:f:h opt
do
    case $opt in
	i) input=$OPTARG;;
	o) output=$OPTARG;;
	f) filter=$OPTARG;;
	h) echo -e $help; exit 0;;
    esac
done

if [ -z $input ]; then
    echo "Please specify an input file (type 'bam2fastq.sh -h' for help)"
    exit 1;
elif [ -z $output ]; then
    echo "Please specify an output file (type 'bam2fastq.sh -h' for help)"
    exit 1;
fi

samtools view $input | grep --no-group-separator $filter | awk '{print "@"$1"_"$12"\n"$10"\n+\n"$11}' >> $output;
