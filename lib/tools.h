#ifndef TOOLS_H
#define TOOLS_H

#include "headers.h"
#include "definitions.h"
#include "kindex.h"
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/aes.h>
#include <openssl/err.h>

#define RSA_KEYLEN 2048
#define AES_KEYLEN 2048


// compare function to sort GenomePosType objects by position
bool gp_compare (GenomePosType i,GenomePosType j);

// calculates the total size of a file in bytes
std::ifstream::pos_type get_filesize(const std::string &fname);

// checks if a directory with the given name exists
bool is_directory(const std::string &path);

// checks if a file exists
bool file_exists(const std::string &fname);

// reads a binary file from hdd and stores its raw content in a char vector
std::vector<char> read_binary_file(const std::string &fname);

// extract the number of reads from a BCL file
uint32_t num_reads_from_bcl(std::string bcl);

// writes a char vector into a binary file
uint64_t write_binary_file(const std::string &fname, const std::vector<char> & data);

std::string getCurrentTimeString();

void consoleOut(std::string msg);
void consoleOut(std::string msg, std::mutex * mtx);

void consoleErr(std::string msg);
void consoleErr(std::string msg, std::mutex* mtx);

//------  Hashing helper functions  ---------------------------------//
HashIntoType hash(const char * kmer, HashIntoType& _h, HashIntoType& _r, AlignmentSettings & settings);
std::string::const_iterator hash_fw(std::string::const_iterator it, std::string::const_iterator end, HashIntoType& _h, AlignmentSettings & settings, bool gapped=true);
//HashIntoType rc(HashIntoType fw); 
/* returns the sequence of a k-mer */
std::string unhash(HashIntoType myHash, unsigned hashLen=K_PriLive);
HashIntoType rc(HashIntoType fw);

// file name construction functions
std::string bcl_name(std::string rt, uint16_t ln, uint16_t tl, uint16_t cl);
std::string alignment_name(uint16_t ln, uint16_t tl, uint16_t cl, uint16_t mt, std::string base);
std::string filter_name(std::string rt, uint16_t ln, uint16_t tl);
std::string position_name(std::string rt, uint16_t ln, uint16_t tl);
std::string sam_tile_name(std::string rt, uint16_t ln, uint16_t tl, uint16_t mate, bool write_bam);
std::string sam_lane_name(std::string rt, uint16_t ln, uint16_t mate, bool write_bam);
std::string lanePath(uint16_t ln);
std::string bcl_path(std::string rt, uint16_t ln, uint16_t cl);

// Function for copying bcl files (copy mode)
void copyBcl(AlignmentSettings* settings, std::string original_path, std::string copy_path, uint16_t lane, uint16_t tile, uint16_t cycle, uint16_t mate, uint16_t delay_sec = 1);
// Encrypt bcl copy files
bool copyFile(AlignmentSettings* settings, std::string file_in, std::string file_out);
// Init data structure for SSL encryption.
bool init_ssl_encryption(AlignmentSettings & settings);
// Encrypt a file using the initialized SSL data structure
bool encrypt_file_ssl(AlignmentSettings & settings, FILE * in, FILE * out);

/** Get the current sequencing cycle using the current alignment cycle and read number.
 * @param cycle The read cycle.
 * @param settings Object containing the program settings.
 * @param read_number The read number (:= index of settings.seqs)
 * @return The sequencing cycle.
 * @author Tobias Loka
 */
uint16_t getSeqCycle(uint16_t cycle, AlignmentSettings* settings, uint16_t read_number=1);

/**
 * Split a string by a delimiter.
 * @param s String to split.
 * @param delim Delimiter for splitting
 * @param elems Vector to push the fragments
 * @return
 * @author Tobias Loka
 */
void split(const std::string &s, char delim, std::vector<std::string> &elems);

// Wobble stuff for ignore alignment
inline bool isWobble(DiffType offset){return offset<=WOBBLE_A && offset>=WOBBLE_I;}
inline bool isSNPWobble(DiffType offset){return offset<=WOBBLE_A && offset>=WOBBLE_T;}
inline bool isInsertionWobble(DiffType offset){return offset == WOBBLE_I;}
inline bool isDeletionWobble(DiffType offset){return offset<=WOBBLE_DA && offset>=WOBBLE_DT;}
uint8_t getWobbleBase(DiffType wobble);
DiffType getWobbleOffset(uint8_t base_id);
HashIntoType wobbleKmer(HashIntoType kmer, CountType position, uint8_t base);

#endif /* TOOLS_H */
