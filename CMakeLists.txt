##############
### Header ###

cmake_minimum_required (VERSION 2.8)
project (PriLive)

# Set the version number 
set (PriLive_VERSION_MAJOR 0)
set (PriLive_VERSION_MINOR 1)

# Set the k-mer length
set (PriLive_K 15 CACHE INT "Set the k-mer length for index and mapper")

# Set flags for compilation
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -g -pthread -W -Wall -std=gnu++11 -O0")


# configure a header file to pass some of the CMake settings to the source code
configure_file (
  "${PROJECT_SOURCE_DIR}/lib/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"  )

# add the binary tree to the search path for include files
include_directories("${PROJECT_BINARY_DIR}")


#############################
### setup SeqAn Library ###

set (CMAKE_MODULE_PATH "/usr/lib/seqan-v2.3.2/util/cmake") # adjust this to [pathToSeqanCode]/util/cmake
set (SEQAN_INCLUDE_PATH "/usr/lib/seqan-v2.3.2/include/") # adjust this to [pathToSeqanCode]/include

# Configure SeqAn, enabling features for libbz2 and zlib.
set (SEQAN_FIND_DEPENDENCIES BZip2)
find_package (SeqAn REQUIRED)

# Add include directories, defines, and flags for SeqAn (and its dependencies).
include_directories (${SEQAN_INCLUDE_DIRS})
add_definitions (${SEQAN_DEFINITIONS})
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${SEQAN_CXX_FLAGS}")


#############################
### setup Boost libraries ###

set(Boost_USE_STATIC_LIBS ON) 
set(Boost_USE_MULTITHREADED ON)  
set(Boost_USE_STATIC_RUNTIME ON) 
find_package( Boost COMPONENTS system filesystem program_options REQUIRED )
include_directories( ${Boost_INCLUDE_DIR} )


############################
### setup Zlib and lz4 libraries ###

find_package( ZLIB REQUIRED )
#set (LZ4_PATH /usr/lib/lz4/lib) # possibly adjust this to [pathToLz4]/lib if using downloaded lz4 source code
include_directories(${LZ4_PATH})
link_directories(${LZ4_PATH})
set(CompressionLibs "${ZLIB_LIBRARIES};lz4")


############################
### setup OpenSSL library ###

#set ( OPENSSL_ROOT_DIR "/usr/lib/openssl-1.0.1c-0" ) # pobbibly adjust this to [pathToLz4]/lib if using downloaded OpenSSL source code. REMARK: Does not work with OpenSSL version >=1.1.0. Tested with version 1.0.1c.

find_package( OpenSSL REQUIRED )
include_directories(${OPENSSL_INCLUDE_DIR})


##############################
### setup PriLive libraries ###

include_directories("${PROJECT_SOURCE_DIR}/lib")

# make a list of PriLive libraries
set (LIB_NAMES tools alnread alnstream illumina_parsers kindex parallel argument_parser ign_alnread ign_alnstream ignore)
set(LIB_LIST "")
foreach (x ${LIB_NAMES})
	list(APPEND LIB_LIST "lib/${x}.cpp")
endforeach()
add_library(PriLiveLibs ${LIB_LIST})


#############################
### Build the executables ###

add_executable (prilive tools/hilive.cpp)
target_link_libraries (prilive PriLiveLibs ${CompressionLibs} ${Boost_LIBRARIES} ${SEQAN_LIBRARIES} ${OPENSSL_LIBRARIES})

add_executable(prilive-build tools/build_index.cpp )
target_link_libraries(prilive-build PriLiveLibs ${CompressionLibs} ${Boost_LIBRARIES} ${SEQAN_LIBRARIES} ${OPENSSL_LIBRARIES})


#####################
### for debugging ###

#get_cmake_property(_variableNames VARIABLES)
#foreach (_variableName ${_variableNames})
	#message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()
