#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include "headers.h"
#include <openssl/evp.h>

// bit representation of A/C/G/T.
#define twobit_repr(ch) ((toupper(ch)) == 'A' ? 0LL : \
                         (toupper(ch)) == 'C' ? 1LL : \
                         (toupper(ch)) == 'G' ? 2LL : 3LL)

// complement bit representation of A/C/G/T.
#define twobit_comp(ch) ((toupper(ch)) == 'A' ? 3LL : \
                         (toupper(ch)) == 'C' ? 2LL : \
                         (toupper(ch)) == 'G' ? 1LL : 0LL)

// bit representation to character
#define revtwobit_repr(n) ((n) == 0 ? 'A' : \
                           (n) == 1 ? 'C' : \
                           (n) == 2 ? 'G' : 'T')

// Allowed characters in sequence
const std::string seq_chars = "ACGTacgt";

// largest number we're going to hash into. (8 bytes/64 bits/32 nt)
// probably 32 bit/16 nt are enough here
typedef uint64_t HashIntoType;

// construct a mask to truncate a binary representation of a k-mer to length K
const HashIntoType MASK = HashIntoType(pow(4,K_PriLive))-1;

// identifiers for genome sequences
typedef uint32_t GenomeIdType;
const GenomeIdType TRIMMED = std::numeric_limits<GenomeIdType>::max();

// list of genome identifiers
typedef std::vector<GenomeIdType> GenomeIdListType;

// list of strings
typedef std::vector<std::string> StringListType;

// position in a genome
typedef int32_t PositionType;

// pair of genome ID and position
struct GenomePosType {
  GenomeIdType gid;
  PositionType pos;

  GenomePosType()=default;
  GenomePosType(GenomeIdType g, PositionType p): gid(g), pos(p) {};
};
//typedef std::tuple<GenomeIdType,PositionType> GenomePosType;

// size of a pair of genome ID and position
const uint64_t GenomePos_size = sizeof(GenomeIdType) + sizeof(PositionType);

// vector of ID:position pairs 
typedef std::vector<GenomePosType> GenomePosListType;

// iterator on GenomePosList
typedef GenomePosListType::iterator GenomePosListIt;

// the k-mer index array
const HashIntoType n_kmer = pow(4,K_PriLive);
typedef std::array<GenomePosListType,n_kmer> KmerIndexType;

// small counters
typedef uint16_t CountType;

// difference between k-mer position in the read and matching position in the reference
typedef int16_t DiffType;

// define a mismatch as max(DiffType)
const DiffType NO_MATCH = std::numeric_limits<DiffType>::max();

// define a trimmed position as max(DiffType)-1
const DiffType TRIMMED_MATCH = std::numeric_limits<DiffType>::max()-1;

/* define SNP wobble offsets */
const DiffType WOBBLE_A = std::numeric_limits<DiffType>::max()-2;
const DiffType WOBBLE_C = std::numeric_limits<DiffType>::max()-3;
const DiffType WOBBLE_G = std::numeric_limits<DiffType>::max()-4;
const DiffType WOBBLE_T = std::numeric_limits<DiffType>::max()-5;

/* define Deletion wobble offsets */
const DiffType WOBBLE_DA = std::numeric_limits<DiffType>::max()-6;
const DiffType WOBBLE_DC = std::numeric_limits<DiffType>::max()-7;
const DiffType WOBBLE_DG = std::numeric_limits<DiffType>::max()-8;
const DiffType WOBBLE_DT = std::numeric_limits<DiffType>::max()-9;

/* define Insertion wobble offset */
const DiffType WOBBLE_I = std::numeric_limits<DiffType>::max()-10;

const DiffType TRIMMED_NOMATCH = std::numeric_limits<DiffType>::max()-11;

// one element in Cigar vector containing match/mismatch information about consecutive k-mers
struct CigarElement {
    CountType length;
    DiffType offset;
    CigarElement (CountType l, DiffType o): length(l), offset(o) {};
    CigarElement (): length(0), offset(NO_MATCH) {};
    std::string toString(){
    	std::ostringstream off;
    	off << length;
    	if ( offset == NO_MATCH )
    		off << "X";
    	else if ( offset<=WOBBLE_A && offset>=WOBBLE_I )
    		off << "W";
    	else
    		off << "M";
    	return off.str();
    }
};

// CigarVector containing CIGAR string like information about the alignments
typedef std::list<CigarElement> CigarVector;
inline std::string toString(CigarVector cig){
	std::ostringstream str;
	for ( auto el = cig.begin(); el!=cig.end(); ++el )
		str << (*el).toString();
	return str.str();
}

/**
 * Struct containing information about the user-defined sequencing reads. This can be barcode or sequence fragments.
 * @author Tobias Loka
 */
struct SequenceElement {

	/** The id of the read. Equals the position in the argument list and in the AlignmentSettings::seqs vector (0-based). */
	CountType id;
	/** The mate number. 0 for barcodes, increasing for sequence reads in the given order (1-based). */
	CountType mate;
	/** The length of the respective read. */
	CountType length;

	/**
	 * Constructor of a SequenceElement NULL object.
	 * @author Tobias Loka
	 */
	SequenceElement () : id(0), mate(0), length(0) {};

	/**
	 * Constructor of a valid SequenceElement object.
	 * @param id The id of the read.
	 * @param m The mate number of the read (0 for barcodes, incrementing for sequence reads)
	 * @param l The length of the read
	 * @author Tobias Loka
	 */
	SequenceElement (CountType id, CountType m, CountType l): id(id), mate(m), length(l) {};

	/**
	 * Check whether the SequenceElement object is a barcode or not.
	 * @return true, if SequenceElement is a barcode. False if not.
	 * @author Tobias Loka
	 */
	bool isBarcode() { return (mate==0);}
};

/** Checks for equality of two SequenceElement objects*/
inline bool operator==(const SequenceElement l, const SequenceElement r) {return (l.length==r.length) && (l.mate==r.mate) && (l.id==r.id);}
/** Checks for inequality of two SequenceElement objects*/
inline bool operator!=(const SequenceElement l, const SequenceElement r) {return !(l==r);}
/** Defines a null-SequenceElement. */
const SequenceElement NULLSEQ = SequenceElement();

// if a CountType field is declared as not activated
const CountType NOT_ACTIVATED = std::numeric_limits<CountType>::max();

// ---------- Ignore alignment stuff ----------

/* Enum to declare encryption types. */
enum class CopyEncryption : uint8_t {
	PLAIN = 0, // plain copies
	SSL= 1, // hybrid encryption (AES256-cbc / RSA) of bcl files
};

/* Data structure for encryption of bcl files. */
struct EvpEncryptionData {
	std::mutex evp_mutex; // required for thread safety
	EVP_PKEY *pub_key = EVP_PKEY_new();
	RSA *rsa_pubkey = NULL;
	EVP_CIPHER_CTX ctx;
	unsigned char *encrypted_key = NULL;
	int encrypted_key_len;
	unsigned char iv[EVP_MAX_IV_LENGTH];
};

enum class IO_type : uint8_t {
	BCL_MANIP = 0,
};

class BCL_Modification_Job {
public:
	std::string fname;
	IO_type type;
	std::vector<uint32_t> reads;
	uint16_t cycle_start;
	uint16_t cycle_end;
	uint16_t lane;
	uint16_t tile;
	BCL_Modification_Job ( std::string f, std::vector<uint32_t> r, uint16_t c_s, uint16_t c_e, uint16_t l, uint16_t t) :
		fname(f), type(IO_type::BCL_MANIP), reads(r), cycle_start(c_s), cycle_end(c_e), lane(l), tile(t) {};
};

/* Data type for the I/O thread */
struct BCL_Modification_Queue{
private:
	std::mutex mutex;
	bool alignment_finished = false;
	std::queue<BCL_Modification_Job> jobs;
public:
	bool push(BCL_Modification_Job job) {
		std::lock_guard<std::mutex> lk(mutex);
		jobs.push(job);
		return true;
	}
	bool pop() {
		std::lock_guard<std::mutex> lk(mutex);
		jobs.pop();
		return true;
	}
	BCL_Modification_Job front() {
		std::lock_guard<std::mutex> lk(mutex);
		BCL_Modification_Job job = jobs.front();
		return job;
	}
	uint32_t jobsSize() {
		return jobs.size();
	}
	void finish() {
		alignment_finished = true;
	}
	bool isFinished() {
		return alignment_finished;
	}
};


// all user parameters are stored in the alignment settings
struct AlignmentSettings {

  // kmer gap pattern
  std::vector<unsigned> kmer_gaps;

  // kmer span
  unsigned kmer_span = K_PriLive;

  // PARAMETER: Base Call quality cutoff, treat BC with quality < bc_cutoff as miscall
  CountType min_qual;

  // PARAMETER: max. insert/deletion size
  DiffType window;

  // PARAMETER: minimum number of errors allowed in alignment
  CountType min_errors;

  // SWITCH: discard One-hit-wonders
  bool discard_ohw;

  // PARAMETER: first cycle to discard one-hit-wonders
  CountType start_ohw;

  // SWITCH: Best-Hit-Mode
  bool any_best_hit_mode;

  // SWITCH: Best-Hit-Mode
  bool all_best_hit_mode;

  // SWITCH: Best-N-Mode
  bool all_best_n_scores_mode;

  // PARAMETER: Best-N-Mode::N
  CountType best_n;

  // PARAMETER: temporary directory for the streamed alignment
  std::string temp_dir;

  // SWITCH: write sam/bam output or not
  bool write_bam=false;

  // SWITCH: Keep the old alignment files of previous cycles
  bool keep_aln_files;

  // PARAMETER: Memory block size for the input and output buffer in the streamed alignment
  uint64_t block_size;

  // PARAMETER: Compression format for alignment files
  uint8_t compression_format;

  // PARAMETER: list of lanes to process
  std::vector<uint16_t> lanes;
  
  // PARAMETER: list of tiles to process
  std::vector<uint16_t> tiles;

  // PARAMETER: root directory of prilive run
  std::string root;

  // PARAMETER: path to the index file
  std::string index_fname;

  // PARAMETER: total number of cycles
  CountType cycles;

  /**
   * Contains the read information of the sequencing machine (as SequenceElement objects). Includes sequence reads and barcodes.
   * Arbitrary numbers and orders of reads are supported. The summed length of all elements must equal the number of sequencing cycles.
   * @author Tobias Loka
   */
  std::vector<SequenceElement> seqs;

  // Number of mates (information taken from the seqLengths parameter)
  uint16_t mates = 0;

  // Define read mates that will not be mapped
  std::vector<uint16_t> exclude_mates;

  //PARAMETER: Stores the barcodes defined by the user. The inner vector contains the single fragments of multi-barcodes.
  std::vector<std::vector<std::string>> barcodeVector;

  // PARAMETER: number of allowed errors for the single barcodes
  std::vector<uint16_t> barcode_errors;

  // SWITCH: if true, keep all barcodes (disables barcode filtering).
  bool keep_all_barcodes;

// PARAMETER: directory in which to create the output directory structure
  std::string out_dir;

  // PARAMETER: number of threads to use
  CountType num_threads;

  /**
   * Get a SequenceElement object from the seqs vector by using the id
   * @param id The id of the SequenceElement.
   * @return The respective SequenceElement object for the given id.
   * @author Tobias Loka
   */
  SequenceElement getSeqById(CountType id) {return seqs[id];}

  /**
   * Get a SequenceElement object from the seqs vector by using the mate number
   * @param id The mate number of the SequenceElement.
   * @return The respective SequenceElement object for the given mate number. NULLSEQ if mate==0 (barcodes).
   * @author Tobias Loka
   */
  SequenceElement getSeqByMate(CountType mate) {
	  if ( mate == 0 ) return NULLSEQ;
	  for (uint16_t i = 0; i != seqs.size(); i++) {
		  if(seqs[i].mate == mate) return seqs[i];
	  }
	  return NULLSEQ;
  }

  uint16_t getMateBySeqCycle(CountType cycle) {

	  // cycle 0 or larger than total number of cycles --> no mate
	  if ( cycle == 0 || cycle > cycles) {
		  return 0;
	  }

	  // Iterate through seqs to find the correct mate
	  uint16_t cycleCounter = 0;
	  auto it = seqs.begin();
	  while ( it != seqs.end() ) {
		  cycleCounter += it->length;
		  if ( cycleCounter >= cycle ) {
			  return it->mate;
		  }
		  ++it;
	  }

	  return 0;
  }
  // ---------- Ignore alignment settings ----------

  // PARAMETER: Path to the ignore index file
  std::string bg_index_fname = "";

  // PARAMETER: Minimal score to discard a read
  CountType bg_min_score = NOT_ACTIVATED;

  // PARAMETER: Maximal score to discard a read
  CountType bg_max_score = NOT_ACTIVATED;

  // PARAMETER: Min. consecutive #matches to detect a "match island" in ignore alignment
  CountType bg_min_island_length;

  // PARAMETER: Min. ignore anchor length
  CountType bg_min_anchor_length;

  // PARAMETER: How often the bcl files get manipulated in ignore alignment
  uint16_t bg_discard_frequency;

  // PARAMETER: Set first cycle to manipulate bcl files in ignore alignment.
  CountType bg_first_cycle_to_discard;

  // PARAMETER: Cycle delay (cycles to wait after the sequencing machine finished a cycle). Only important for bcl manipulation (prevent conflicts with machine)
  uint16_t cycleDelay;
  bool decreasing_delay = false;

  // PARAMETER: With how many errors in main alignment the ignore alignment starts
  CountType bg_min_errors_start;

  // PARAMETER: Minimal errors in main alignment to allow discarding a read
  CountType bg_min_errors_discard = NOT_ACTIVATED;

  // PARAMETER: Maximal errors in main alignment to allow discarding a read
  CountType bg_max_errors_discard = 0;

  // PARAMETER: Cycle to reach maximal score and discard number in dynamic ignore alignment
  CountType bg_dynamic_max_cycle = NOT_ACTIVATED;

  // PARAMETER: First cycle of dynamic parameter setting
  CountType bg_dynamic_min_cycle = NOT_ACTIVATED;

  //PARAMETER: Number of valid barcodes before discarding a read (default: all)
  CountType bg_barcodes_before_discard;

  // PARAMETER: path to copy the original bcl files in copy mode
  std::string copy_dir = "";

  // PARAMETER: Type of encryption for the bcl file copies
  CopyEncryption encryptionType = CopyEncryption::SSL;

  // PARAMETER: encryption password
  std::string pubKeyPath = "/home/lokat/public_key.pem";

  // DATA: Data structure for ssl encryption (shared by all threads)
  EvpEncryptionData evp_envelope;

  // MUTEX: mutex for cont file output (to pretend line merges)
  std::mutex cont_file_mutex;

  // MUTEX: mutex for console output (to pretend line merges)
  std::mutex console_mutex;

  // THREAD: thread for writing bcl files
  BCL_Modification_Queue io_queue;

  // FUNCTION: Method to check for ignore alignment existance
  bool hasIgnoreAlignment(){ return bg_max_score != NOT_ACTIVATED; };

};


#endif /* DEFINITIONS_H */
