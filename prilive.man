.\" Manpage for PriLive
.TH man 7 "19 July 2017" "0.1" "prilive man page"

.SH NAME
PriLive: Privacy-preserving real-time filtering of Illumina reads 

.SH SYNOPSIS
.B prilive [options] BC_DIR INDEX CYCLES [OUTDIR]

.SH DESCRIPTION
PriLive is a software to detect and remove human or other unwanted reads when the sequencing machine is still running.
PriLive is suitable for Illumina HiSeq machines or similar.

.SH POSITIONAL OPTIONS
.IP BC_DIR
Path to the BaseCalls directory of the sequencing machine. This directory mus be organized by Lanes, Cycles and Tiles in a fixed hierarchical structure. Please find all detail for this data structure in the user manual of bcl2fastq (version 1.8.4)which is provided by Illumina.
.IP INDEX
Path to the foreground index file (.kix). The index must be created from a (multi-)FASTA file using prilive-build before running PriLive. For details, type \'prilive-build --help\'.
.IP CYCLES
Total number of sequencing cycles. This includes all read fragment and barcode lengths. PriLive will finish when the last cycle was handled.
.IP OUTDIR
Directory to save all output files of the foreground alignment (SAM/BAM) and the filtering. This does not influence the modification of the base call files (-bcl) which is performed at the location of BC_DIR (see BC_DIR).

.SH "GENERAL OPTIONS"

.IP -h,--help 
Print help message and exit.
 Usage:
.B -h

.IP --license
Print license and disclaimer and exit.
 Usage:
.B --license
 
.IP --accept-disclaimer
Confirm that you read, understood and accept the disclaimer of PriLive (type \'prilive --license\' to read). If not set, the disclaimer must be accepted manually when starting the program.
 Usage:
.B --accept-disclaimer
 
.SH "SEQUENCING SETUP"

.IP "-l,--lanes	[Default: 1-8]"
The lanes that are considered in PriLive. An Illumina sequencing run can consist of several lanes with different samples. Usually, PriLive will not be used for all samples of a sequencing run. With the -l option you can select one or several lanes that are handled by PriLive.
 Usage: 
.B "-l 2 4 5"

.IP "-t,--tiles	[Default: 96 tiles; [1-2][1-3][01-16] ]"
Each lane consists of several tiles. Tiles are 4-digit numbers that may differ for different machines and modes in Illumina sequencing. With -t, you can select the lane numbers that exist in your sequencing run.
 Usage:
.B "-t 1101 1102 1103"
 In bash, you can use the \'seq\' command:
.B -t $(seq 1101 1116)

.IP -r,--reads
The -r parameter is used to describe the structure of the sequencing results. The structure consists of several elements that can be either reads (R) or barcodes (B). Every read is analyzed separately by PriLive while barcodes are used for the identification of the sample of interest. The sum of all fragments must equal the total number of cycles as declared in the CYCLES option. If the -r parameter is omitted, the sequence is assumed to be one single read fragment.
 Usage:
.B "-r 101R 8B 8B 101R"

.IP -b,--barcodes
The -b parameter defines the barcodes of our sample. Barcodes with several fragments (e.g. dual barcodes) are concetenated with a "-". The number of nucleotides must equal the length of the respective fragment.
 Usage:
.B -b AATT-CCGG -b TTAA-GGCC [for two dual-barcodes of length 2x4bp, e.g. in the sequence structure -r 100R 4B 4B 100R]

.IP -E,--barcode-errors
Describes the number of errors toleratd for each barcode fragment. Must have as many parameters as there are barcode fragments defined by -r.
 Usage:
.B -E 2 2

.IP --keep-all-barcodes
If set, all reads that do not match the defined barcode sequences are also aligned and filtered by PriLive.
 Usage:
.B --keep-all-barcodes

.SH "I/O SETTINGS"

.IP --temp
Directory to store temporary files. By default, this is the original Illumina BaseCalls directory.
 Usage:
.B --temp /path/to/temp/dir

.IP -k,--keep-files
If set, the temporary alignment files are not deleted after each cycle. ! Settings this option can lead to huge disk space requirements !
 Usage:
.B --keep-files

.IP --copy
Activates the functionality to copy the original base call files before they are handled (and modified) by PriLive. You also have to declare the target directory here.
 Usage:
.B --copy /path/to/copy/dir

.IP --copy-enc
If --copy is used, it can be chosen to store the original data in an encrypted or unencrypted manner. SSL encryption is activated by default.
 REMARK: We strongly (!) recommend to NOT create unencrypted copies when using PriLive for privacy protection!
 Usage:
.B --copy-enc [ ssl | plain ] 

.IP --pub-key
The location of the public key that is used for SSL encryption of the original base call files (if activated).
 Usage:
.B --pub-key /path/to/pub-key

.SH "FOREGROUND ALIGNMENT SETTINGS"

.IP -e,--min-errors
Number of errors tolerated in the foreground alignment. By default, this parameter also strongly influences the filtering results. For metagenomic data (when using no foreground reference genome), set this value to 0.
 Usage:
.B -e 4

.IP "--any-best-hit | -H,--all-best-hit | -N,--all-best-n-scores | -A,--all-hits"
Mode of the foreground alignment output. Select to report one (randomly selected) best alignment (--any-best-hit; default), all best alignments (--all-best-hit), all alignments of the best N alignment scores (--all-best-n-scores) or all alignments that are found (--all-hits).
 Usage:
.B [ | -H | -N | -A ]

.IP --disable-ohw-filter
By default, single k-mer hits that cannot be extended in the subsequent cycles are assumed to be "by chance" (One-Hit Wonders / OHW). Thus, these alignment candidates are removed. Set --disable-ohw-filter to inactivate this behavior. This may notably increase the runtime of PriLive.
 Usage:
.B --disable-ohw-filter

.IP "--start-ohw	[Default: K+5]"
Define the cycle when OHWs are removed for the first time.
 Usage:
--start-ohw 20

.IP "-w,--window	[Default: 5]"
In the seed extension step, k-mers can match a positions that does not perfectly match the expected position for this seed (e.g., due to small InDels). Thus, PriLive looks for matching k-mers in a window around the expected position. The size of this window can have effects on the alignment accuracy.
 Usage:
-w 3

.IP "--min-quality	[Default: 1]"
Define the minimal quality of a base call to be considered for the foreground alignment.
 Usage:
--min-quality 5

.SH "BACKGROUND ALIGNMENT / READ FILTERING SETTINGS"

.IP -f,--bg
Path to the background index file (.kix). The index must be created from a (multi-)FASTA file using prilive-build before running PriLive. For details, type \'prilive-build --help\'. All reads that are aligned to this reference genome and not aligned to the foreground reference genome with the defined criteria are irrecoverably removed from the original base call files!
 Usage:
-f /path/to/background/index

.IP "--bg-score [Default: auto]"
Minimum background alignment score for a read to be filtered. This is the number of matches in a local alignment that is not interrupted by more than 1 consecutive mismatches (SNPs or InDels of length 1bp).
 Usage:
--bg-score 35

.IP "--bg-anchor [Default: K+4]"
Minimum anchor length (consecutive matches at the beginning of a seed) for the background alignment. A higher value leads to a significantly lower runtime, a lower value to higher sensitivity.
 Usage:
--bg-anchor 20

.IP "--bg-islands [Default: 3]"
Minimum number of matches after a mismatch in a background alignment. If another mismatch occurs earlier, the alignment will be discarded. A higher value leads to a lower runtime, a lower value to higher sensitivity.
 Usage:
--bg-islands 1

.IP "--bg-start [Default: auto]"
Only if the minimum number of errors in the foreground alignments of a read is greater or equal the value of --bg-start, the background alignment starts in parallel for this read. A higher value leads to a lower runtime, a lower value to higher sensitivity. A value of 0 means that the background alignment starts in parallel for all reads from the beginning.
 Usage:
--bg-start 1

.IP "--bg-discard [Default: auto]"
Only if the minimum number of errors in the foreground alignments of a read is greater or equal the value of --bg-discard, a read can be removed from the raw base call files. One or two values can be added to this option. If two values are given to this option, the value increases between the cycles declared in --bg-dynamic-cycles. Thus, in a later stage of the sequencing procedure, more errors are necessary to discard a read. Higher values mean higher specificity and lower sensitivity, lower values the other way around.
 Usage:
--bg-discard 2 4

.IP "--bg-dynamic-cycles [Default: auto]"
Defines the cycle span where the parameters of --bg-discard and --bg-score increase (linear). If only one value is given to this parameter, this will be the last cycle of the parameter changes. The first cycle is then computed automatically. Dynamic parametrization in general provides the possibility to filter detected reads in an earlier stage of the sequencing procedure without having too sensitive parameters for the rest of the algorithm.
 Usage:
--bg-dynamic-cycles 35 80

.IP "--bg-freq [Default: 10]"
The original base call files are only modified every X cycles. A higher value leads to a higher delay in the actual removal of sequence information but can heavily reduce overall I/O time.
 Usage:
--bg-freq 5

.IP "--delay [Default: 0]"
Activate a delay (in cycles) for the modification of base call files. This can be used if technical problems with the sequencing machine occur because of the base call file modification. The delay is ignored in the last sequencing cycle.
 Usage:
--delay 5

.IP "--bg-min-barcodes [Defualt: all]"
Defines how many barcodes be correct before a read is allowed to be removed from the base call files. With this option, it is for example possible to only consider a barcode that was manually put at the beginning of the read instead of waiting for the regular Illumina Barcodes to be sequenced.
 Usage:
--bg-min-barcodes 1

.SH BUGS
- You cannot have an option that allows for multiple values (e.g. -l) directly before the positional arguments.
.SH AUTHOR
Tobias P. Loka and the PriLive contributors (see CONTRIBUTORS file for details).