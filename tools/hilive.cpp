#include "../lib/headers.h"
#include "../lib/definitions.h"
#include "../lib/kindex.h"
#include "../lib/alnstream.h"
#include "../lib/parallel.h"
#include "../lib/argument_parser.h"
#include "../lib/ign_alnstream.h"

std::string license =
"Copyright (c) 2017, Tobias P. Loka & the PriLive contributors. See CONTRIBUTORS for more info.\n"
"All rights reserved.\n"
"\n"
"Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:\n"
"\n"
"1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.\n"
"\n"
"2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.\n"
"\n"
"3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.\n"
;
// the worker function for the threads
void worker (TaskQueue & tasks, TaskQueue & finished, TaskQueue & failed, AlignmentSettings* settings, KixRun* idx, KixRun* ign_idx, IgnoreAlignmentController* ign_control, bool & surrender ) {

    // loop that keeps on running until the surrender flag is set
    while ( !surrender ) {
        // try to obtain a new task
        Task t = tasks.pop();
        bool fileex = file_exists(bcl_name(t.root, t.lane, t.tile, getSeqCycle(t.cycle, settings, t.seqEl.id)));
        if ( t != NO_TASK && fileex) {
            // Execute the task
            bool success = true;

            try {

            	// copy bcl file if copy option is selected
            	if(settings->copy_dir!="")
            		copyBcl(settings, t.root, settings->copy_dir, t.lane, t.tile, t.cycle, t.seqEl.id);

                if ( std::find(settings->exclude_mates.begin(), settings->exclude_mates.end(), t.seqEl.mate) != settings->exclude_mates.end() ) {
                	std::ostringstream out;
                	out << getCurrentTimeString() << ": Ignoring Task [" << t << "] due to exclude-mates parameter.";
                	consoleOut(out.str(), &settings->console_mutex);
                	finished.push(t);
                    std::this_thread::sleep_for (std::chrono::milliseconds(100));
                    continue;
                }

                // Seed extension if current read is no barcode.
                if ( !t.seqEl.isBarcode() ) {
                	ign_control->align(t, idx, ign_idx);

                // If current read is barcode, extend barcode sequence in all sequence read align files
                } else {
                	CountType handled_mates = ign_control->extend_barcode(t.lane, t.tile, t.cycle, t.seqEl.id, settings);
                	std::ostringstream out;
                	out << getCurrentTimeString() << ": Task [" << t << "]: Extended barcode of " << handled_mates << " mates.";
                	consoleOut(out.str(), &settings->console_mutex);
                }
            }

            catch (const std::exception &e) {
            	std::ostringstream out;
            	out << getCurrentTimeString() << ": ";
                out << "Failed to finish task [" << t << "]: " << e.what();
                consoleErr(out.str(), &settings->console_mutex);
                success = false;
            }

            if (success) {
                finished.push(t);
            }
            else {
            	std::this_thread::sleep_for (std::chrono::seconds(300));
                failed.push(t);
            }

        }
        else {
        	if ( t != NO_TASK )
        		failed.push(t);
        }

        // send this thread to sleep for a second
        std::this_thread::sleep_for (std::chrono::milliseconds(100));
    }  

}


// create a special SAM worker, that writes out a SAM file for a tile
void sam_worker (TaskQueue & tasks, AlignmentSettings* settings, KixRun* idx) {

    // loop that keeps on running until the surrender flag is set
    while ( true ) {
        // try to obtain a new task
        Task t = tasks.pop();
        if ( t != NO_TASK ) {
            // Execute the task if mate not ignored
        	alignments_to_sam(t.lane,t.tile,t.root,t.seqEl.length,t.seqEl.mate,idx,settings);
        }
        else {
            return;
        }
    }  

}


void IO_worker (AlignmentSettings* settings) {

	BCL_Modification_Queue* io_queue = &(settings->io_queue);

	while ( ! ( io_queue->isFinished() && io_queue->jobsSize() == 0 ) ) {

		// Pause if no jobs to do.
		if ( io_queue->jobsSize() == 0 ) {
			std::this_thread::sleep_for( std::chrono::seconds(15));
			continue;
		}

		BCL_Modification_Job job = io_queue->front();
		bool success = false;

		success = removeFromBcl(&job, settings);

		if ( success ) {
			io_queue->pop();
		} else {
			io_queue->push(job);
			io_queue->pop();
		}

		std::this_thread::sleep_for ( std::chrono::milliseconds(100) );
	}
}



int main(int argc, const char* argv[]) {
    time_t t_start = time(NULL);

    // parse the command line arguments, store results in settings
    AlignmentSettings settings;
    if (parseCommandLineArguments(settings, license, argc, argv)) // returns true if error occured
        return 1;

    // load the index
    std::cout << "Loading Index" << std::endl;
    KixRun* index = new KixRun();
    index->deserialize_file(settings.index_fname);

    // load the ignore index
    KixRun* index_ign;

    // Ignore alignment controller
    IgnoreAlignmentController* ign_control;
    if(settings.bg_index_fname!=""){
    	if(settings.index_fname!=settings.bg_index_fname){
    		std::cout << "Loading Ignore Index" << std::endl;
    		index_ign = new KixRun();
    		index_ign->deserialize_file(settings.bg_index_fname);
    	} else {
    		std::cout << "Ignore Index equals reference index  - don't load again!" << std::endl;
    		index_ign = index;
    	}
    }


    // Create the overall agenda
    Agenda agenda (settings.root, settings.cycles, settings.lanes, settings.tiles, &settings);


    // prepare the alignment
    std::cout << "Initializing Alignment files. Waiting for the first cycle to finish." << std::endl;
    bool first_cycle_available = false;

    // wait for the first cycle to be written. Attention - this loop will wait infinitely long if no first cycle is found
    while ( !first_cycle_available ) {
        // check for new BCL files and update the agenda status
        agenda.update_status();

        // check if the first cycle is available for all tiles
        first_cycle_available = true;
        for ( auto ln : settings.lanes ) {
            for ( auto tl : settings.tiles ) {
                if ( agenda.get_status(Task(ln,tl,settings.getSeqById(0),1,"")) != BCL_AVAILABLE) {
                    first_cycle_available = false;
                }
            }
        }

        // take a small break
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    std::cout << "First cycle complete. Starting alignment." << std::endl;

    // write empty alignment file for each tile and for each sequence read
    for (uint16_t ln : settings.lanes) {
        for (uint16_t tl : settings.tiles) {
        	CountType mate = 1;
        	for ( ; mate <= settings.mates; mate++ ) {
        		StreamedAlignment s (ln, tl, settings.root, settings.getSeqByMate(mate).length);
        		s.create_directories(&settings);
        		s.init_alignment(mate, &settings);
        	}
        }
    }

    ign_control = new IgnoreAlignmentController(settings.root, &settings, settings.lanes, settings.tiles);

    if (settings.temp_dir != "" && !is_directory(settings.temp_dir)){
        std::cerr << "Error: Could not find temporary directory " << settings.temp_dir << std::endl;
        return -1;
    }

    // Set up the queues
    TaskQueue toDoQ;
    TaskQueue finishedQ;
    TaskQueue failedQ;

    // Create the threads
    std::cout << "Creating " << settings.num_threads << " alignment threads." << std::endl;
    bool surrender = false;
    std::vector<std::thread> workers;
    for (int i = 0; i < settings.num_threads; i++) {
        workers.push_back(std::thread(worker, std::ref(toDoQ), std::ref(finishedQ), std::ref(failedQ), &settings, index, index_ign, ign_control, std::ref(surrender)));
    }

    std::cout << "Creating 1 I/O thread." << std::endl;
    std::thread io_thread = std::thread(IO_worker, &settings);

    // Process all tasks on the agenda
    while ( !agenda.finished() ) {

        // check for new BCL files and update the agenda status
        //agenda.update_status(settings.decreasing_delay, settings.cycleDelay);
    	agenda.update_status();

        // fill the To Do queue with tasks from the agenda
        while(true) {
            Task t = agenda.get_task();
            if (t == NO_TASK)
                break;
            toDoQ.push(t);
            agenda.set_status(t,RUNNING, &settings);
        }

        // take a look in the finished queue and process finished tasks
        while(true) {
            Task t = finishedQ.pop();
            if (t == NO_TASK)
                break;
            agenda.set_status(t,FINISHED, &settings);
        }

        // take a look in the failed queue and process failed tasks
        while(true) {
            Task t = failedQ.pop();
            if (t == NO_TASK)
                break;
            if (agenda.get_status(t) == RUNNING) {
                // give it one more chance
                agenda.set_status(t,RETRY, &settings);
                toDoQ.push(t);
            }
            else {
                agenda.set_status(t,FAILED,&settings);
                std::cout << "Task failed! " << t << std::endl;
            }

        }

        // take a small break
        std::this_thread::sleep_for (std::chrono::milliseconds(100));
    }  

    // Halt the threads
    surrender = true;
    for (auto& w : workers) {
        w.join();
    }
    std::cout << "All threads joined." << std::endl;
    settings.io_queue.finish();

    
    std::cout << "Writing SAM files." << std::endl;
    // Create individual SAM files for every tile
    TaskQueue sam_tasks; 
    std::vector<Task> tv = agenda.get_SAM_tasks();
    for ( auto t: tv ) {
        sam_tasks.push(t);
    }

    workers.clear();
    for (int i = 0; i < settings.num_threads; i++) {
        workers.push_back(std::thread(sam_worker, std::ref(sam_tasks), &settings, index));
    }

    for (auto& w : workers) {
        w.join();
    }

    std::cout << "Waiting for I/O thread to finish." << std::endl;
    io_thread.join();
    std::cout << "Total run time: " << time(NULL) - t_start << " s" << std::endl;
    delete index;
    return 1;
}
